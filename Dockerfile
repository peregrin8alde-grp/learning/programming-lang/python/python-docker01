# FROM 専用の ARG があればここで設定
FROM python:3

# FROM 以降で使いたい ARG の設定
ARG GID=1000
ARG GRPNAME=devgroup
ARG UID=1000
ARG USERNAME=devuser

WORKDIR /usr/src/app

COPY ./src/requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt \
    && groupadd -g ${GID} ${GRPNAME} \
    && useradd -m -s /bin/bash -u ${UID} -g ${GID} ${USERNAME}

USER ${USERNAME}

COPY ./src .

CMD [ "python3", "./main.py" ]
