# python-docker01

Docker + pip + VSCode(devcontainer) を使った開発サンプル。フレームワークとして NumPy を利用。

- Docker イメージ : https://hub.docker.com/_/python
- pip : https://pip.pypa.io/en/stable/
- NumPy : https://numpy.org/doc/stable/index.html

基本、以下の資材を揃えてアプリ用イメージをビルドして利用する。

- アプリのソース
- `requirements.txt`
- `Dockerfile`
  - 各種インストール作業は root で実施した後、コンテナ実行ユーザとして一般ユーザを用意

アプリが必要とするパッケージのビルドが済んだ状態でイメージを作成することで管理／実行が楽になる。

簡易なアプリであれば、以下（ Docker イメージの説明にあるもの）のようにアプリソースだけで実行するのもあり。

```
$ docker run -it --rm --name my-running-script -v "$PWD":/usr/src/myapp -w /usr/src/myapp python:3 python your-daemon-or-script.py
```


## アプリ用イメージの作成

```
# ビルド
## `--force-rm=true` はビルド失敗時にゴミが残るのを避けるために指定
docker build -t my-python-app --force-rm=true .

# 実行
docker run -it --rm --name my-running-app my-python-app

# 削除
docker rmi my-python-app
```


